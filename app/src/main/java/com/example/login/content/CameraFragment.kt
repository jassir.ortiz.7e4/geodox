package com.example.login.content

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Camera
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.login.content.map.REQUEST_CODE_LOCATION
import com.example.login.databinding.FragmentCameraBinding

private const val MY_CAMERA_REQUEST_CODE = 100

class CameraFragment : Fragment() {
    lateinit var binding: FragmentCameraBinding
    lateinit var photoButton: Button
    lateinit var camera: Camera

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCameraBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        photoButton = binding.photoButton

        photoButton.setOnClickListener {

        }
    }

    /*private fun enableCamera() {
        if (!::camera.isInitialized) return
        if (()) {
            camera. = true
        } else {
            requestCameraPermission()
        }
    }

    private fun requestCameraPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            Toast.makeText(
                requireContext(),
                "Habilita los permisos de camara",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION
            )
        }*/

    fun takePhoto() {

    }
}