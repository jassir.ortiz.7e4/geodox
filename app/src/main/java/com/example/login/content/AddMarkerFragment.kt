package com.example.login.content

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.login.R
import com.example.login.content.data.MarkerData
import com.example.login.databinding.FragmentAddMarkerBinding
import com.example.login.databinding.FragmentCameraBinding
import com.google.android.gms.maps.model.Marker
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage

class AddMarkerFragment : Fragment() {
    private val db = FirebaseFirestore.getInstance()
    lateinit var binding: FragmentAddMarkerBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddMarkerBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val latitud = arguments?.getFloat("latitud")
        val longitud = arguments?.getFloat("longitud")

        binding.buttonSave.setOnClickListener {
            addMarker(MarkerData(null, title = binding.TitleText.text.toString(), lat = latitud!!.toDouble(), long = longitud!!.toDouble()))

            findNavController().navigate(R.id.mapFragment)
        }
    }

    fun addMarker(marker: MarkerData) {
        //marker.image = uploadPicture(Uri.parse(marker.photoDirectory))

        db.collection("Markers").add(marker).addOnSuccessListener {
            println("Marcador creado: ${it.id}")
        }.addOnFailureListener { e ->
            println(e.message)
        }
    }
}