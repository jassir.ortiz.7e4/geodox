package com.example.login.content.data

data class MarkerData(
    var id: String? = null,
    var title: String? = null,
    var lat: Double? = null,
    var long: Double? = null
)