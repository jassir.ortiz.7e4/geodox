package com.example.login.content.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.login.R
import com.example.login.databinding.FragmentRegisterBinding
import com.google.firebase.auth.FirebaseAuth

class Register : Fragment() {
    private lateinit var binding: FragmentRegisterBinding
    lateinit var registerButton: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerButton = binding.registerButton

        registerButton.setOnClickListener {
            registerUser()
        }
    }

    fun registerUser() {
        val email = binding.EmailAddress.text.toString()
        val password = binding.Password.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()) {
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val emailRegister = it.result?.user?.email
                        returnMap()
                    } else {
                        Toast.makeText(this.context,"Error al registrar el usuario!", Toast.LENGTH_LONG).show()
                    }
                }
        } else {
            Toast.makeText(this.context,"Los campos no fueron rellenados", Toast.LENGTH_LONG).show()
        }
    }

    fun returnMap() {
        findNavController().navigate(R.id.logIn)
    }
}