package com.example.login.content.map

import android.Manifest
import android.content.pm.PackageManager
import android.icu.text.CaseMap
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.login.R
import com.example.login.content.data.MarkerData
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

const val REQUEST_CODE_LOCATION = 100

class MapFragment : Fragment(), OnMapReadyCallback {
    private val db = FirebaseFirestore.getInstance()
    lateinit var map: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.google_map, container, false)
        createMap()
        return rootView
    }

    override fun onResume() {
        super.onResume()
        if (!::map.isInitialized) return
        if (!isLocationPermissionGranted()) {
            map.isMyLocationEnabled = false
            Toast.makeText(
                requireContext(), "ACEPTA LOS PERMISOS DE GEOLOCALIZACIÓN",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            Toast.makeText(
                requireContext(),
                "Habilita los permisos de geolocalización",
                Toast.LENGTH_SHORT
            ).show()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_CODE_LOCATION
            )
        }
    }

    fun createMap() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        println("Mapa copiado")
        markerITB()
        readMarkers()
        enableLocation()

        map.setOnMapLongClickListener {
            createMarkerFirebase(it)
        }
    }

    fun createMarker(latLng: LatLng, title: String) {
        val myMarker = MarkerOptions().position(latLng).title(title)
        map.addMarker(myMarker)
    }

    // Read markers on firebase
    fun readMarkers() {
        CoroutineScope(Dispatchers.IO).launch {
            db.collection("Markers").addSnapshotListener(object : EventListener<QuerySnapshot> {
                override fun onEvent(value: QuerySnapshot?, error: FirebaseFirestoreException?) {

                    if (error != null) {
                        Log.e("Firestore error", error.message.toString())
                        return

                    }
                    getList()
                }
            })
        }
    }

    fun getList(){
        db.collection("Markers").get().addOnSuccessListener {
            for (document in it) {
                val newMarker = document.toObject(MarkerData::class.java)
                newMarker.id = document.id
                println(document.id)
                createMarker(
                    LatLng(
                        newMarker.lat!!.toDouble(),
                        newMarker.long!!.toDouble()
                    ), newMarker.title!!
                )
            }
        }
    }

    fun createMarkerFirebase(latLng: LatLng) {
        val action = MapFragmentDirections.actionMapFragmentToAddMarkerFragment(
            latLng.latitude.toFloat(),
            latLng.longitude.toFloat()
        )
        findNavController().navigate(action)
    }

    fun markerITB() {
        val coordinates = LatLng(41.4534227, 2.1841046)
        val myMarker = MarkerOptions().position(coordinates).title("ITB")
        map.addMarker(myMarker)
        map.animateCamera(
            CameraUpdateFactory.newLatLngZoom(coordinates, 18f),
            5000, null
        )
    }

    private fun isLocationPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun enableLocation() {
        if (!::map.isInitialized) return
        if (isLocationPermissionGranted()) {
            map.isMyLocationEnabled = true
        } else {
            requestLocationPermission()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED
            ) {
                map.isMyLocationEnabled = true
            } else {
                Toast.makeText(
                    requireContext(), "Accepta els permisos de geolocalització",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}