package com.example.login.content.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.login.R
import com.example.login.databinding.FragmentLoginBinding
import com.google.firebase.auth.FirebaseAuth


class LogIn : Fragment() {
    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.signinButton.setOnClickListener {
            userRegister()
        }

        binding.loginButton.setOnClickListener {
            logInUser()
        }
    }

    fun logInUser() {
        val email = binding.EmailAddress.text.toString()
        val password = binding.PasswordLog.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty()) {
            FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        val emailLogIn = it.result?.user?.email
                        returnMap()
                    } else {
                        Toast.makeText(
                            this.context,
                            "No se logro iniciar sesión, intentelo de nuevo",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }
        } else {
            Toast.makeText(this.context,"Error al iniciar sesión! Verifique el correo y la contraseña", Toast.LENGTH_LONG).show()
        }
    }

    fun userRegister(){
        findNavController().navigate(R.id.register)
    }

    fun returnMap() {
        findNavController().navigate(R.id.mapFragment)
    }
}