package com.example.login.content

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.login.databinding.FragmentCameraBinding
import com.example.login.databinding.ListMarkerFramentBinding

class ListMarkerFragment: Fragment() {
    lateinit var binding: ListMarkerFramentBinding

    lateinit var buttonExit: Button
    lateinit var buttonViewMarker: Button

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = ListMarkerFramentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }


}